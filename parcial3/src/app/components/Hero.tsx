import Image from "next/image";
import * as React from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
export default function Hero(props:{title:string;subtitle:string;image:string}){
    return(
        <div>
            <h1 className="title">{props.title}</h1>
            <p className="p">{props.subtitle}</p>
            <Image src={props.image} alt="" width={916} height={1013} className="imagen"></Image>
            <div className="boton2">
            <Button variant="contained">Today</Button>
            </div>
            
            <p className="p">The EuroLeague Finals Top Scorer is the individual award for the player that gained the highest points in the EuroLeague Finals</p>
            
            <Stack spacing={2} direction="row">
            <Button variant="contained" className="boton3">CONTINUE READING</Button>
            </Stack>
        </div>
    )
}