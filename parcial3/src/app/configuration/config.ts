import axios from "axios";

export const instansePokemon = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_POKEMON,
    timeout: 5000,
    headers: {'X-Custom-Header': 'foobar'}
});

instansePokemon.interceptors.request.use(function(config){
    return config;
}, function (error){
    return Promise.reject(error);
})