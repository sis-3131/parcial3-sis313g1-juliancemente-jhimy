"use client"
import * as React from 'react';
import AppBar from './components/AppBar';
import Hero from './components/Hero';
import Image from 'next/image';
import { Button, Grid} from '@mui/material';
import Card from './components/CardServices';
import CardServices from './components/CardServices';
import { getPokemon } from './services/pokemon.services';
import { usePokemon } from './hook/usePokemon';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
export default function Home() {
  const {pokemon,loading} = usePokemon()
  console.log(loading? "cargando...":"")
  if(!loading){
    console.log(pokemon)
  }
  
  return (
    <div>
      <AppBar></AppBar>
      <Hero title={'TOP SCORER TO THE FINAL MATCH'} subtitle={''} image={'/images/ball.svg'}></Hero>
      <div className="jugador">
        <Image src="/images/jugador.svg" alt="" width={850} height={945.57}></Image>
      </div>
<Button></Button>
{pokemon?.results.map((item: any, index: number) => {
                const splitUrl = item.url.split('/')
                const id = splitUrl[splitUrl.length - 2]
                return (
                  
                    <CardServices 
                      title={item.name}
                      image={`${process.env.NEXT_PUBLIC_IMAGES_POKEMON}/${id}.png`}
                      name='Ethiopian runners took the top four spots.'
                      date='Jan 10, 2022 ∙ 3 min read' >
                    </CardServices >
                  
                )
              })}
            <div>
              <h1 className='titulo2'>Category</h1>
              </div>
              <Grid container>
                <div className='title2'>
                  <h1>Football</h1>
                </div>
                <div className='title3'>
                <h1>car sport</h1>
                </div>
                <div className='title4'>
                  <h1>Football</h1>
                </div>
                <div className='title5'>
                <h1>car sport</h1>
                </div>
              </Grid>
    </div>
  );
}
