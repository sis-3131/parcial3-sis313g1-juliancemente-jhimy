import { instansePokemon } from "../configuration/config"

export const getPokemon = async () =>{
   const response = await instansePokemon.get ('/pokemon/?limit=2&offset=0')    
   return response.data
}